all: pxattr 

PXATTROBJS = main.o pxattr.o 
pxattr:  $(PXATTROBJS)
	$(CXX) -o pxattr $(PXATTROBJS) -lstdc++
clean:
	rm -f \#* *~ *.o pxattr
